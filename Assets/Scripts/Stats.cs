using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats : MonoBehaviour
{
    public Teams teamID;

    public int level;
    public int EXP;
    public int maxHP;
    public int currentHP;
    public int attack;
    public int defense;
}