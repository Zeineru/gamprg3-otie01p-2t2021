using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Tower : MonoBehaviour
{
    [SerializeField]
    private int HP;

    [SerializeField]
    private int damage;

    void Awake()
    {
        HP = 5000;
        damage = 300;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public int GetHP()
    {
        return HP;
    }

    public float GetDamage()
    {
        return damage;
    }
}