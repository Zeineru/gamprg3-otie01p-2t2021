using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    private GameObject owner;
    private float velocity;
    private float damage;

    public Rigidbody rb;
    public Teams ownerTeamID;

    // Start is called before the first frame update
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody>();

        //ownerTeamID = owner.gameObject.GetComponent<Target>().teamID;
    }

    // Update is called once per frame
    void Update()
    {
        Destroy(this, 3.0f);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<Target>() != null)
        {
            if(ownerTeamID != collision.gameObject.GetComponent<Target>().teamID)
            {
                collision.gameObject.GetComponent<Target>().ApplyDamage(damage);
            }
        }

        else
        {
            //Destroy(this, 10.0f);
        }
    }

    public void AssignValues(GameObject owner, float velocity, float damage)
    {
        this.owner = owner;
        this.velocity = velocity;
        this.damage = damage;
    }
}