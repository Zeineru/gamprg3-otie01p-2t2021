using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerState
{
    Idle,
    Turning,
    Moving,
    Attacking,
    Stop
};

public class Player : MonoBehaviour
{
    [SerializeField]
    private PlayerState currentState;

    [SerializeField]
    private Teams teamID;

    [SerializeField]
    private Stats stats;

    private Projectile projectileFired;

    public Transform spawnPoint;

    public Projectile projectilePrefab;

    public float projectileVelocity = 1000.0f;

    void Awake()
    {
        currentState = PlayerState.Idle;
        stats.teamID = Teams.Player;

        stats.level = 1;
        stats.EXP = 0;

        stats.maxHP = 5000;
        stats.currentHP = stats.maxHP;
        stats.attack = 750;
        stats.defense = 450;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (stats.currentHP < stats.maxHP)
        {
            HPRegeneration();
        }


        if (Input.GetMouseButton(0))
        {
            RangeAttack();
        }
    }

    private void HPRegeneration()
    {
        int regenValue = (int)(stats.maxHP * 0.07f);

        if (stats.currentHP + regenValue >= stats.maxHP)
        {
            stats.currentHP = stats.maxHP;
        }

        else
        {
            stats.currentHP += (int)(regenValue * Time.deltaTime);
        }
    }

    private void RangeAttack()
    {
        Vector3 XZDir = new Vector3(gameObject.transform.forward.x, 0.0f, gameObject.transform.forward.z);

        XZDir.Normalize();

        projectileFired = Instantiate(projectilePrefab, this.transform.position, Quaternion.identity);

        projectileFired.AssignValues(this.gameObject, projectileVelocity, stats.attack);

        projectileFired.transform.position = spawnPoint.transform.position;

        projectileFired.transform.forward = gameObject.transform.forward;

        projectileFired.GetComponent<Rigidbody>().velocity = XZDir * projectileVelocity;

        Debug.Log("XZDir :" + XZDir);
        Debug.Log("Velocity :" + projectileFired.GetComponent<Rigidbody>().velocity);
    }
}