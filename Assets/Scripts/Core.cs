using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Core : MonoBehaviour
{
    public Teams teamsID;

    [SerializeField]
    private int maxHP;

    [SerializeField]
    private int currentHP;

    // Start is called before the first frame update
    void Start()
    {
        maxHP = 5000;
        currentHP = maxHP;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        
    }

    public void ApplyDamage(float damage)
    {
        this.currentHP -= (int)damage;
    }
}
