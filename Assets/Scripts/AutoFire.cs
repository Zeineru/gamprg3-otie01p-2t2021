using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoFire : MonoBehaviour
{
    [SerializeField]
    private int HP;

    [SerializeField]
    private float damage;

    private Projectile projectileFired;

    public Tower towerParent;

    public Transform spawnPoint;

    public Projectile projectilePrefab;

    public float projectileVelocity = 1000.0f;

    void Awake()
    {
        if (towerParent == null)
        {

            towerParent = transform.parent.gameObject.GetComponent<Tower>();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        SetInitialValues();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionEnter(Collision collision)
    {
        Fire();
    }

    private void SetInitialValues()
    {
        HP = towerParent.GetHP();
        damage = towerParent.GetDamage();
    }

    private void Fire()
    {
        Debug.Log("Firing!!!");

        projectileFired = Instantiate(projectilePrefab, this.transform.position, Quaternion.identity);

        //projectileFired.AssignValues(this.gameObject, projectileVelocity, damage);

        projectileFired.transform.position = spawnPoint.transform.position;

        projectileFired.transform.forward = gameObject.transform.forward;

        projectileFired.GetComponent<Rigidbody>().velocity = projectileFired.transform.forward * projectileVelocity;
    }
}