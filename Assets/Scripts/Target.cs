using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Teams
{
    None,
    Player,
    Opponent
};

public class Target : MonoBehaviour
{
    private Stats statsReference;

    public Teams teamID;

    public GameObject parent;

    void Awake()
    {
        statsReference = parent.GetComponent<Stats>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ApplyDamage(float damage)
    {
        statsReference.currentHP -= (int)damage;
    }
}