using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitSpawn : MonoBehaviour
{
    public GameObject unit;

    void Awake()
    {
        //public Player newUnit = Instantiate<Player>(unit.GetComponent<Player>());
        
        Instantiate(unit, this.transform.position, Quaternion.identity);
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
