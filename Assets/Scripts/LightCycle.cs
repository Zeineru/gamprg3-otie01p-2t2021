using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class LightCycle : MonoBehaviour
{
    public Light directionalLight;

    public Color dayLightColor;
    public Color nightLightColor;

    public float timePerCycle;
    public float timeCount;
    public bool isItDay = true;

    // Start is called before the first frame update
    void Start()
    {
        //directionalLight.color = dayLightColor;
    }

    // Update is called once per frame
    void Update()
    {
        timeCount += Time.deltaTime;

        if(timeCount >= timePerCycle)
        {
            isItDay = !isItDay;
            timeCount = 0;
        }

        if(isItDay == true)
        {
            directionalLight.color = Color.Lerp(dayLightColor, nightLightColor, Time.realtimeSinceStartup / timePerCycle);
            //directionalLight.intensity = Mathf.Lerp(1.0f, 1.5f, Time.realtimeSinceStartup / timePerCycle);
        }

        else
        {
            directionalLight.color = Color.Lerp(nightLightColor, dayLightColor, Time.realtimeSinceStartup / timePerCycle);
            //directionalLight.intensity = Mathf.Lerp(1.5f, 1.0f, Time.realtimeSinceStartup / timePerCycle);
        }
    }
}