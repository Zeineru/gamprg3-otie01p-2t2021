using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public float cameraSpeed = 20.0f;
    public float screenBorderSize = 10.0f;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 position = transform.position;

        // North

        if (Input.mousePosition.y >= Screen.height - screenBorderSize)
        {
            position.z += cameraSpeed * Time.deltaTime;
        }

        // South

        if (Input.mousePosition.y <= screenBorderSize)
        {
            position.z -= cameraSpeed * Time.deltaTime;
        }

        // West

        if (Input.mousePosition.x >= screenBorderSize)
        {
            position.x += cameraSpeed * Time.deltaTime;
        }

        // East

        if (Input.mousePosition.x <= Screen.height - screenBorderSize)
        {
            position.x-= cameraSpeed * Time.deltaTime;
        }

        transform.position = position;
    }
}