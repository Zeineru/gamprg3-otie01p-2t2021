using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerMovement : MonoBehaviour
{
    private NavMeshAgent agent;

    [Range(0.01f, 1.0f)]
    public float rotationSpeed;

    public float rotationVelocity;

    // Start is called before the first frame update
    void Start()
    {
        agent = gameObject.GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(1))
        {
            RaycastHit hit;

            if(Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, Mathf.Infinity))
            {
                agent.SetDestination(hit.point);

                Quaternion facing = Quaternion.LookRotation(hit.point - transform.position);

                float rotationY = Mathf.SmoothDampAngle
                                  (transform.eulerAngles.y, facing.eulerAngles.y, 
                                  ref rotationVelocity, rotationSpeed * (Time.deltaTime * 5.0f));

                transform.eulerAngles = new Vector3(0, rotationY, 0);
            }
        }
    }
}